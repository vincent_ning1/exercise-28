﻿using System;
using System.Collections.Generic;

namespace exercise_28
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
            
            var stu1 = new Student("Mike",20,10010690);
            stu1.Papers = new List<string>{};
            stu1.Papers.Add("It Infastructure");
            stu1.Papers.Add("Software Packages");
            stu1.Papers.Add("Introductory to Programming");
            stu1.Papers.Add("Professional Skills");
            stu1.SayHello("Mike",20);
            stu1.ListAllpaper();
            stu1.PrintFavouritePaper(stu1.Papers[3]);
            Console.WriteLine("\n\n\n\n");

            var stu2 = new Student("Sara",18,10010691);
            stu2.Papers = new List<string>{};
            stu2.Papers.Add("It Infastructure");
            stu2.Papers.Add("Software Packages");
            stu2.Papers.Add("Introductory to Programming");
            stu2.Papers.Add("Professional Skills");
            stu2.SayHello("Sara",18);
            stu2.ListAllpaper();
            stu2.PrintFavouritePaper(stu1.Papers[0]);
            Console.WriteLine("\n\n\n\n");

            var teacher1 = new Teacher("Ray",30,5001,"Professional Skills");
            teacher1.ListPaper("Professional Skills");
            Console.WriteLine("\n");
            var teacher2 = new Teacher("Jeff",25,5002,"Introductory to Programming");
            teacher2.ListPaper("Professional Skills");
            Console.WriteLine("\n");
            var teacher3 = new Teacher("Stefan",40,5004,"It Infastructure");
            teacher3.ListPaper("Professional Skills");
            Console.WriteLine("\n");
            var teacher4 = new Teacher("Murray",45,5008,"Software Packages");
            teacher4.ListPaper("Professional Skills");
            Console.WriteLine("\n\n\n\n");

            
            var p = new List<Papers>();
            p.Add( new Papers { PaperCode = "COMP5001", PaperDescription = "Professional Skills" });
            
            p.Add( new Papers { PaperCode = "COMP5002", PaperDescription = "Intro to programing" });
            
            p.Add( new Papers { PaperCode = "COMP5004", PaperDescription = "It Infastructure" });
            
            p.Add( new Papers { PaperCode = "COMP5008", PaperDescription = "Software Packages" });
            int x = 0;
            foreach( var Papers in p)
            {
                
                Console.WriteLine("-----------------------------------------");
                Console.WriteLine($"|PaperCode = {p[x].PaperCode}");
                Console.WriteLine($"|PaperDescription ={p[x].PaperDescription }");
                Console.WriteLine("-----------------------------------------");
                x++;
            }

            


            

            
            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }

}
