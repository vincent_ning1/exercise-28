   using System;
    public class Person     //The first class is person
    {
       protected string Name = "";//- Name : String
       protected int Age ; //- Age : Int

    //    public Person()
    //    {

    //    }
        public  Person(string name, int age)//+ Constructor : String, Int
        {
            Name = name;
            Age = age;
        }
        public void SayHello(string name,int age)//+ SayHello() : Void
        {
            Console.WriteLine($"Hello!\nMy name is {Name},I am {Age} years old");
        }
    }