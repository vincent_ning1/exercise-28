using System;
namespace exercise_28
{
    class Teacher : Person//create a third class calledTeacher, which is a subclass of person
    {
        int TeacherId;//- TeacherId: Int
        string Papers;//- Paper : string

        public Teacher(string name, int age,int teacherId,string papers) :base(name,age) // Constructor : String, Int, String, Int
        {
            TeacherId = teacherId;
            Papers = papers;
        }
        public void ListPaper(string Papers)//+ ListPaper(): void (print with name and paper)
        {
            Console.WriteLine($"{Name} teaches {Papers}");
        }

        //  Create 4 teachers each with the paper they teach
    }

}
